using battle_ship.db;
using battle_ship.framework;
using battle_ship.net;
using TcpClient = System.Net.Sockets.TcpClient;

namespace battle_ship.events
{
    public class User : Event
    {
        public Operation AddUser(TcpServer server, TcpClient client, Operation op)
        {
            var user = op.DeserializePayload<model.User>();

            var game = DataBase.GetGameByIpAndPort(server.IpAddress.ToString(), server.Port);

            if (game.Users[0] == null)
            {
                game.Users[0] = new model.User(user.Name, game.FieldSize);
                op.Payload = game.Users[0].Field;
            }
            else
            {
                game.Users[1] = new model.User(user.Name, game.FieldSize);
                op.Payload = game.Users[1].Field;
            }

            return op;
        }
    }
}