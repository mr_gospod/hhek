using System.Collections.Generic;

namespace battle_ship.framework
{
    public class ViewHandler
    {
        public ViewHandler()
        {
            Views = new Dictionary<string, View>();
        }

        public Dictionary<string, View> Views { get; set; }

        public void Print(string view)
        {
            Views[view]?.Render();
        }
    }
}