using System;
using System.Text.Json;

namespace battle_ship.framework
{
    [Serializable]
    public class Operation
    {
        public Operation()
        {
        }

        public Operation(string action, object payload)
        {
            Type = payload.GetType().Name;
            Action = action;
            Payload = payload;
        }
        
        public string Type { get; set; }
        public string Action { get; set; }
        public object Payload { get; set; }

        public T DeserializePayload<T>()
        {
            return JsonSerializer.Deserialize<T>(((JsonElement) Payload).GetRawText());
        }
    }
}