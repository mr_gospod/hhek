using System.Text.Json;
using battle_ship.net;
using TcpClient = System.Net.Sockets.TcpClient;

namespace battle_ship.framework
{
    public abstract class Event
    {
        public Operation Handle(TcpServer server, TcpClient client, string msg)
        {
            var op = JsonSerializer.Deserialize<Operation>(msg);

            var method = GetType().GetMethod(op.Action);

            var response = (Operation) method?.Invoke(this, new object[] {server, client, op});

            if (response != null)
                response.Type = response.Payload.GetType().Name;

            return response;
        }
    }
}