using System;
using System.Linq;
using System.Reflection;

namespace battle_ship.framework
{
    public static class Util
    {
        public static ViewHandler LoadViews(string viewNameSpace)
        {
            return LoadViews(new Terminal(), new ViewHandler(), new DataHolder(), viewNameSpace);
        }

        public static ViewHandler LoadViews(ViewHandler viewHandler, string viewNameSpace)
        {
            return LoadViews(new Terminal(), viewHandler, new DataHolder(), viewNameSpace);
        }

        public static ViewHandler LoadViews(Terminal terminal, ViewHandler viewHandler, DataHolder dataHolder,
            string viewNameSpace)
        {
            var nameSpace = typeof(Program).Namespace + "." + viewNameSpace;

            var views = Assembly.GetExecutingAssembly().GetTypes().Where(t => t.IsClass && t.Namespace == nameSpace);

            views.ToList().ForEach(v =>
            {
                Console.WriteLine("Loading view: " + v.Name);

                try
                {
                    var type = Type.GetType(nameSpace + "." + v.Name);

                    if (type == null) return;

                    if (!(Activator.CreateInstance(type) is View view)) return;

                    view.Init(terminal, viewHandler, dataHolder);
                    viewHandler.Views.Add(v.Name, view);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            });

            return viewHandler;
        }

        public static EventHandler LoadEvents(string eventNameSpace)
        {
            return LoadEvents(new EventHandler(), eventNameSpace);
        }

        public static EventHandler LoadEvents(EventHandler eventHandler, string eventNameSpace)
        {
            var nameSpace = typeof(Program).Namespace + "." + eventNameSpace;

            var events = Assembly.GetExecutingAssembly().GetTypes().Where(e => e.IsClass && e.Namespace == nameSpace);

            events.ToList().ForEach(e =>
            {
                Console.WriteLine("Loading event: " + e.Name);

                try
                {
                    var type = Type.GetType(nameSpace + "." + e.Name);

                    if (type == null) return;

                    if (!(Activator.CreateInstance(type) is Event _event)) return;

                    eventHandler.Events.Add(e.Name, _event.Handle);
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception);
                }
            });

            return eventHandler;
        }
    }
}