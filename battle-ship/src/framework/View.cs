namespace battle_ship.framework
{
    public abstract class View
    {
        public Terminal Terminal { get; set; }
        public ViewHandler Views { get; set; }
        public DataHolder DataHolder { get; set; }

        public void Init(Terminal terminal, ViewHandler viewDictionary, DataHolder dataHolder)
        {
            Views = viewDictionary;
            Terminal = terminal;
            DataHolder = dataHolder;
        }

        public abstract void Render();
    }
}