using System.Linq;
using battle_ship.framework;
using battle_ship.model;

namespace battle_ship.view
{
    public class PlaceShips : View
    {
        private void PreRender()
        {
            Terminal.Clear();

            PrintField();

            Terminal.PrintSeparator();
        }

        public override void Render()
        {
            PreRender();

            Terminal.PrintCenter("Select row:");
            var row = Terminal.ReadCenter();
            Terminal.PrintCenter("Select col:");
            var col = Terminal.ReadCenter();
        }

        private void PrintField()
        {
            // TODO: always need to cast right, maybe better way?
            foreach (var row in ((User) DataHolder.Data).Field)
                Terminal.PrintCenter(string.Concat(row.Select(c => c + " ")));
        }
    }
}