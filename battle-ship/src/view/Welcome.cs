using System;
using System.Collections.Generic;
using battle_ship.framework;
using battle_ship.model;
using battle_ship.net;
using Util = battle_ship.util.Util;

namespace battle_ship.view
{
    public class Welcome : View
    {
        private readonly controller.Welcome _controller;

        public Welcome()
        {
            _controller = new controller.Welcome(this);
        }

        private void PreRender()
        {
            Terminal.Clear();
        }

        public override void Render()
        {
            PreRender();

            var options = new List<Tuple<string, Action<string>>>
            {
                new Tuple<string, Action<string>>("Create Game", CreateGame),
                new Tuple<string, Action<string>>("Join Game", JoinGame)
            };

            Terminal.SelectOption(options, null);
        }

        private void CreateGame(string sel)
        {
            Terminal.Clear();

            Terminal.PrintCenter("Enter field size:");

            _controller.CreateGame(int.Parse(Terminal.ReadCenter()));
            Terminal.PrintCenter("Game created!");

            Views.Print("Welcome");
        }

        private void JoinGame(string sel)
        {
            Terminal.Clear();

            Terminal.PrintCenter("Select game to join:");

            // TODO: replace by discovery service
            var options = Util.ReadLines("server.list");

            var game = Terminal.SelectOption(options, () => Views.Print("Welcome"));


            if (game.Equals("")) return;


            Terminal.Clear();

            Terminal.PrintCenter("Enter name:");

            var user = new User
            {
                Name = Terminal.ReadCenter(),
                Client = new TcpClient(game.Split(":")[0], int.Parse(game.Split(":")[1]))
            };

            var response = user.Client.Write("AddUser", user);

            user.Field = response.DeserializePayload<char[][]>();

            DataHolder.Data = user;

            Views.Print("PlaceShips");
        }
    }
}