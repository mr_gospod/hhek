using System.Net.Sockets;
using System.Text;
using System.Text.Json;
using battle_ship.framework;

namespace battle_ship.net
{
    public class TcpClient
    {
        private readonly System.Net.Sockets.TcpClient _client;
        private readonly NetworkStream _stream;

        public TcpClient()
        {
        }

        public TcpClient(string targetIp, int targetPort)
        {
            _client = new System.Net.Sockets.TcpClient(targetIp, targetPort);
            _stream = _client.GetStream();
        }

        public void Close()
        {
            _stream.Close();
            _client.Close();
        }

        public Operation Read()
        {
            var data = new byte[256];
            var bytes = _stream.Read(data, 0, data.Length);
            var msg = Encoding.ASCII.GetString(data, 0, bytes);

            return JsonSerializer.Deserialize<Operation>(msg);
        }

        public Operation Write<T>(string action, T obj)
        {
            var op = new Operation(action, obj);
            var data = Encoding.ASCII.GetBytes(JsonSerializer.Serialize(op));
            _stream.Write(data, 0, data.Length);

            return Read();
        }
    }
}