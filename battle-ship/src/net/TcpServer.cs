using System.Net;
using System.Net.Sockets;
using System.Text.Json;
using System.Threading;
using battle_ship.framework;
using static System.Text.Encoding;

namespace battle_ship.net
{
    public class TcpServer
    {
        public TcpServer(int clientLimit, EventHandler eventHandler)
        {
            IpAddress = IPAddress.Parse(Util.GetLocalIp());
            Port = Util.GetFreePort();
            Listener = new TcpListener(IpAddress, Port);
            ClientLimit = clientLimit;

            Listener.Start();

            for (var i = 0; i < ClientLimit; i++) AcceptClient(eventHandler);
        }

        public IPAddress IpAddress { get; }
        public int Port { get; }
        private TcpListener Listener { get; }
        public int ClientLimit { get; }

        public void Close()
        {
            Listener.Stop();
        }

        private void AcceptClient(EventHandler eventHandler)
        {
            new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;

                var client = Listener.AcceptTcpClient();
                var stream = client.GetStream();
                var bytes = new byte[256];

                while (stream.Read(bytes) != 0)
                    stream.Write(ASCII.GetBytes(JsonSerializer.Serialize(
                        eventHandler.Handle(this, client, ASCII.GetString(bytes)))));

                stream.Close();
                client.Close();
            }).Start();
        }
    }
}