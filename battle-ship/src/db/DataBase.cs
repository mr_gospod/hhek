using System.Collections.Generic;
using System.Linq;
using battle_ship.model;
using battle_ship.util;

namespace battle_ship.db
{
    public static class DataBase
    {
        private static readonly List<Game> Games = new List<Game>();

        public static List<Game> GetGames()
        {
            return Games;
        }

        public static void InsertGame(Game game)
        {
            if (GameExists(game.Server.IpAddress.ToString(), game.Server.Port)) return;
            Games.Add(game);
            // TODO: replace by discovery service
            Util.WriteLine("server.list", game.Server.IpAddress + ":" + game.Server.Port);
        }

        public static void UpdateGame(Game game)
        {
            var i = Games.FindIndex(g =>
                g.Server.IpAddress.ToString().Equals(game.Server.IpAddress.ToString()) &&
                g.Server.Port.Equals(game.Server.Port));

            if (i > -1) Games[i] = game;
        }

        public static Game GetGameByIpAndPort(string ip, int port)
        {
            return Games.Find(u => u.Server.IpAddress.ToString().Equals(ip) && u.Server.Port.Equals(port));
        }

        private static bool GameExists(string ip, int port)
        {
            return Games.Any(game => game.Server.IpAddress.ToString().Equals(ip) && game.Server.Port.Equals(port));
        }
    }
}