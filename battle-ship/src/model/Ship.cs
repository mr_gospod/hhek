namespace battle_ship.model
{
    public class Ship
    {
        public Ship()
        {
        }

        public Ship(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int X { get; set; }
        public int Y { get; set; }
    }
}