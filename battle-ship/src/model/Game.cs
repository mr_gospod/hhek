using battle_ship.net;

namespace battle_ship.model
{
    public class Game
    {
        public Game(TcpServer server, int fieldSize)
        {
            Server = server;
            Users = new User[Server.ClientLimit];
            FieldSize = fieldSize;
        }

        public TcpServer Server { get; }
        public User[] Users { get; }
        public int FieldSize { get; }
    }
}