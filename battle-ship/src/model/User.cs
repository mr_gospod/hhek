using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using battle_ship.net;

namespace battle_ship.model
{
    [Serializable]
    public class User
    {
        public User()
        {
        }

        // TODO: or get fieldSize from db via server ip port
        public User(int fieldSize)
        {
            Field = new char[fieldSize][];
            Ships = new List<Ship>();

            CreateField(fieldSize);
        }

        public User(string name, int fieldSize)
        {
            Name = name;
            Field = new char[fieldSize][];
            Ships = new List<Ship>();

            CreateField(fieldSize);
        }

        public string Name { get; set; }
        public List<Ship> Ships { get; set; }
        public char[][] Field { get; set; }
        [JsonIgnore] public TcpClient Client { get; set; }

        private void CreateField(int fieldSize)
        {
            for (var i = 0; i < fieldSize; i++)
                Field[i] = new char[fieldSize];

            for (var i = 0; i < fieldSize; i++)
            for (var j = 0; j < fieldSize; j++)
                if (i != 0 && j == 0)
                    Field[i][j] = (char) (64 + i);
                else if (j != 0 && i == 0)
                    Field[i][j] = (char) (48 + j);
                else
                    Field[i][j] = ' ';
        }
    }
}