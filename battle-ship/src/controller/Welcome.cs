using battle_ship.db;
using battle_ship.framework;
using battle_ship.model;
using battle_ship.net;
using Util = battle_ship.framework.Util;

namespace battle_ship.controller
{
    public class Welcome
    {
        private View _view;

        public Welcome(View view)
        {
            _view = view;
        }

        public void CreateGame(int fieldSize)
        {
            var eventHandler = Util.LoadEvents("events");
            DataBase.InsertGame(new Game(new TcpServer(2, eventHandler), fieldSize));
        }
    }
}