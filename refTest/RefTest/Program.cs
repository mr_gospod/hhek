﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RefTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Class2 obj2 = new Class2();
            Class1 obj1 = new Class1(obj2);
            Console.WriteLine("Main obj2 = " + obj2);
        }
    }
}
