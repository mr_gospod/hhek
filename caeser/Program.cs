﻿using System;

namespace Caeser
{
    internal static class Program
    {
        public static void Main(string[] args)
        {
//            var str = new CryptString("abc def ABC DEF Was geht Zoo zoo ya", 1);
            var str = new CryptString(args[0], int.Parse(args[1]));
            
            Console.WriteLine("Encoded = " + str.Encrypt());
            Console.WriteLine("IsEncrypted = " + str.IsEncrypted);
            Console.WriteLine("Decoded = " + str.Decrypt());
        }
    }

    internal class CryptString
    {
        private string Str { get; set; }
        private int Offset { get; }
        public bool IsEncrypted { get; private set; }

        public CryptString(string str, int offset)
        {
            Str = str;
            Offset = offset;
        }
        
        public string Decrypt()
        {
            var buff = "";

            foreach (var c in Str)
            {
                if (c == ' ') {
                    buff += ' ';
                    continue;
                }

                var cOff = c - Offset % 26;

                if (c >= 65 && cOff < 65 || c >= 97 && cOff < 97)
                    buff += (char)(cOff + 26);
                else
                    buff += (char)cOff;
            }

            Str = buff;
            IsEncrypted = false;
            return buff;
        }

        public string Encrypt()
        {
            var buff = "";

            foreach (var c in Str)
            {
                if (c == ' ') {
                    buff += ' ';
                    continue;
                }

                var cOff = c + Offset % 26;

                if (c <= 90 && cOff > 90 || c <= 122 && cOff > 122)
                    buff += (char)(cOff - 26);
                else
                    buff += (char)cOff;
            }

            Str = buff;
            IsEncrypted = true;
            return buff;
        }
    }
}