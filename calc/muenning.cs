// by Martin Münning

using System;

namespace Calculator {

    class Calc {

        static void Main() {
            double firstNum = 0, secondNum = 0, result = 0;

            do {
                Console.Clear();
                Console.WriteLine("Enter 1st num:");
                firstNum = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Enter 2nd num:");
                secondNum = Convert.ToDouble(Console.ReadLine());
                
                Console.WriteLine("Select operation: 1=Add | 2=Sub | 3=Mul | 4=Div");
                switch (Convert.ToInt32(Console.ReadLine())) {
                    case 1:
                        result = addNum(firstNum, secondNum);
                        break;
                    case 2:
                        result = subNum(firstNum,secondNum);
                        break;
                    case 3:
                        result = mulNum(firstNum,secondNum);
                        break;
                    case 4:
                        result = divNum(firstNum,secondNum);
                        break;
                    default:
                        Console.WriteLine("Not a valid operation!\n");
                        break;
                }

                Console.WriteLine("Result: " + result + "\n");
                Console.WriteLine("Enter 0 to exit or any other num to continue.\n");                
            } while (Convert.ToInt32(Console.ReadLine()) != 0);
        }

        static double addNum(double x, double y) {
            return x+y;
        }

        static double subNum(double x, double y) {
            return x-y;
        }

        static double mulNum(double x, double y) {
            double tmp = 0;
            
            for (double i = y; i < x; x--) {
                tmp += y;
            }

            return tmp;
        }

        static double divNum(double x, double y) {
            double retVal = 0.0;

            if (y != 0) {
                retVal = x / y;
            }

            return retVal;
        }
    }
}
